const _express = require("express");
const _path = require("path");
const join = _path.join;
const DIST_FOLDER = join(process.cwd(), "build");
const router = _express.Router();

const fixtureTv = require("../fixture/tv-data.json");
const fixtureSelection = require("../fixture/tv.selection.ts");
//auth
router.post("/login", (req, res) => {
  const {
    body: { login, pwd }
  } = req;
  if (login === "toto" && pwd === "rerere") {
    const nonce = Math.random().toString(36);
    return res.status(200).json({ token: nonce });
  }
  return res.status(401).json({ message: "invalid creds" });
});

// Server static files from /browser

router.get("/tv", (req, res) => {
  const ApiResponse = {
    ...fixtureTv,
    status: 200
  };
  return res.status(200).json(ApiResponse);
});

router.get("/tv-selection", (req, res) => {
  const ApiResponse = {
    ...{ data: fixtureSelection },
    status: 200
  };
  return res.status(200).json(ApiResponse);
});

router.get("*", (req, res) => {
  res.sendFile(join(DIST_FOLDER, "./index.html"));
});

module.exports = router;
