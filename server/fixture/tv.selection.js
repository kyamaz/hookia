var SELECTION_FIXTURE = [
    {
        value: "id",
        viewValue: "id",
        isSelected: false
    },
    {
        value: "name",
        viewValue: "name",
        isSelected: true
    },
    {
        value: "image",
        viewValue: "image",
        isSelected: true
    },
    {
        value: "season",
        viewValue: "season",
        isSelected: true
    },
    {
        value: "number",
        viewValue: "episod",
        isSelected: true
    },
    {
        value: "runtime",
        viewValue: "runtime",
        isSelected: true
    },
    {
        value: "airdate",
        viewValue: "airdate",
        isSelected: true
    },
    {
        value: "airstamp",
        viewValue: "airstamp",
        isSelected: false
    },
    {
        value: "airtime",
        viewValue: "airtime",
        isSelected: false
    },
    {
        value: "summary",
        viewValue: "summary",
        isSelected: true
    },
    {
        value: "url",
        viewValue: "details",
        isSelected: true
    },
    {
        value: "_links",
        viewValue: "api",
        isSelected: true
    }
];
module.exports = SELECTION_FIXTURE;
//# sourceMappingURL=tv.selection.js.map