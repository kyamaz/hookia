const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const path = require("path");
const session = require("express-session");
const cors = require("cors");

class Server {
  public app: any;
  public server: any;
  public root: string;
  public port: number;
  private secret: string = "secretsecret";
  public options: any;
  public token: any;
  public http;
  private appRoutes;

  // Bootstrap the application.
  public static bootstrap(): Server {
    return new Server();
  }
  constructor() {
    this.http = http;
    this.options = {};

    // Create expressjs application
    this.app = express();

    // Configure application
    this.config();

    // Setup routes
    this.routes();

    // Create server
    this.server = this.http.createServer(this.app);

    // Start listening

    this.listen();
  }

  // Configuration
  private config(): void {
    // By default the port should be 8080
    this.port = parseInt(process.env.PORT) || 4500;
    //cors
    this.app.use(cors());
    // Initialize Passport
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(
      session({ secret: "adminAdmin", resave: false, saveUninitialized: false })
    );
  }

  // Configure routes
  private routes(): void {
    const DIST_FOLDER = path.join(process.cwd(), "build");
    this.app.set("view engine", "html");
    this.app.set("views", DIST_FOLDER);
    this.app.use(express.static(DIST_FOLDER));
    this.app.use("/", require("./routes/api.ts"));
  }

  // Start HTTP server listening
  private listen(): void {
    //listen on provided ports
    this.server.listen(this.port);
    //add error handler
    this.server.on("error", error => {
      console.log("ERROR", error);
    });
    //start listening on port
    this.server.on("listening", () => {
      console.log(
        "==> Listening: Open up http://localhost:%s/ in your browser.",
        this.port
      );
    });
  }
}

function returnEnv() {
  return process.env.NODE_ENV;
}
// Bootstrap the server
Server.bootstrap();
module.exports = Server;
