import * as React from "react";
import { TableHeaderProps } from "react-virtualized";
import { DraggableItem } from "@components/DraggableItem/DraggableItem";
import { TableActions } from "@components/TableActions/TableActions";
import { Header } from "@model/index";

export function dataHeaderRender<T extends any>(
  headerProps: TableHeaderProps,
  props: T
): JSX.Element {
  const { headers, onHandleSetWidths, widths } = props;
  return (
    <DraggableItem
      vHeader={headerProps}
      headers={headers}
      handleSetWidths={onHandleSetWidths}
      widths={widths}
    />
  );
}

export function actionHeaderRender<T extends any>(
  headerProps: TableHeaderProps,
  props: T
): JSX.Element {
  const { allHeaders, headers, onHandleSetWidths, widths } = props;
  return <TableActions vHeader={headerProps}  headers={headers} allHeaders={allHeaders}/>;
}
interface DragList {
  isSelected: Array<Header>;
  isNotSelected: Array<Header>;
}

function insert<T>(arr:Array<T>, index:number, newItem:T):Array<T> {
  return[
  // part of the array before the specified index
  ...arr.slice(0, index),
  // inserted item
  newItem,
  // part of the array after the specified index
  ...arr.slice(index)
]
}
export function makeHeadersList(
  selectedList: Array<Header>,
  fullList: Array<Header>
): DragList {
  return fullList.reduce(
    (acc: DragList, curr: Header): DragList => {
      if (selectedList.findIndex(l => l.value === curr.value) !== -1) {
        const idx=selectedList.findIndex(l => l.value === curr.value)
        return { ...acc, ...{ isSelected: insert(acc.isSelected, idx, curr) }}
      }
      return {
        ...acc,
        ...{ isNotSelected: [...acc.isNotSelected, ...[curr]] }
      };
    },

    {
      isSelected: [],
      isNotSelected: []
    }
  );
}
