import * as React from "react";

import { TableCellProps } from "react-virtualized";
import { ListText, ListLink } from "@ui/list";
import { ListPopover } from "@components/ListPopover/ListPopover";

export function renderContent({ cellData }: TableCellProps): JSX.Element {
  return cellData;
}

// displayed data

function _setComponent(key: string, data: any): JSX.Element {
  switch (true) {
    case key === "url":
      const { url } = data;
      const content = (
        <ListLink href={data} data-testid="url-content">
          {data}
        </ListLink>
      );
      return (
        <ListPopover
          content={content}
          trigger={<p data-testid="url-trigger"> more</p>}
          dataKey="summary"
        />
      );
    case key === "image": {
      const content = <img src={data.medium} data-testid="image-content" />;

      return (
        <ListPopover
          content={content}
          trigger={<p data-testid="image-trigger"> preview</p>}
          dataKey="image"
        />
      );
    }

    case key === "summary": {
      const content = data.substring(
        data.lastIndexOf("<p>") + 3,
        data.lastIndexOf("</p>")
      );

      return (
        <ListPopover
          content={<p data-testid="summary-content">{content}</p>}
          trigger={<ListText data-testid="summary-trigger">{content}</ListText>}
          dataKey="summary"
        />
      );
    }
    case key === "_links": {
      const {
        self: { href }
      } = data;
      const content = (
        <ListLink href={href} data-testid="_links-content">
          {href}
        </ListLink>
      );

      return (
        <ListPopover
          content={content}
          trigger={<p data-testid="_links-trigger"> api link</p>}
          dataKey="_links"
        />
      );
    }
    case key === "airstamp": {
      return (
        <ListText data-testid="airstamp">
          {new Date(data).toLocaleDateString()}
        </ListText>
      );
    }
    default:
      return <ListText>{data}</ListText>;
  }
}
export function displayData(
  keys: Array<string>,
  data: any
): { [key: string]: JSX.Element } {
  return keys.reduce(
    (acc: { [key: string]: JSX.Element }, curr: string) => ({
      ...acc,
      ...{ [curr]: _setComponent(curr, data[curr]) }
    }),
    {}
  );
}
