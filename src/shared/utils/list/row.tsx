import * as React from "react";

import { TableRowProps } from "react-virtualized";
import { SortableElement } from "react-sortable-hoc";
import { defaultTableRowRenderer } from "react-virtualized";

//https://github.com/bvaughn/react-virtualized/issues/1063
interface FixedTableRowProps extends TableRowProps {
  i: number;
}
const SortableTableRowRenderer = SortableElement<FixedTableRowProps>(
  ({ i, ...props }): any => {
    props.index = i;
    return defaultTableRowRenderer(props as FixedTableRowProps);
  }
) as any;

export function rowRender({ index, ...props }: TableRowProps): JSX.Element {
  return (
    <SortableTableRowRenderer
      index={index}
      data-testid={`row-${index}`}
      i={index}
      {...props}
    />
  );
}
