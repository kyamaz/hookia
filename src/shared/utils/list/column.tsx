import * as React from "react";

import { TableHeaderProps, Column } from "react-virtualized";
import { ColumnType, Header } from "@model/index";
import { actionHeaderRender, dataHeaderRender, renderContent } from "../list";

const actionColumn = (props: any, header: Header, width: number): JSX.Element => (
  <Column
    label={header.viewValue}
    headerRenderer={(headerProps: TableHeaderProps) =>
      actionHeaderRender(headerProps, props)
    }
    dataKey={header.value}
    key={header.value}
    width={width}
  />
);
const dataColumn = (props: any, header: Header, width: number): JSX.Element => (
  <Column
    headerRenderer={(headerProps: TableHeaderProps) =>
      dataHeaderRender(headerProps, props)
    }
    cellRenderer={renderContent}
    key={header.value}
    label={header.viewValue}
    dataKey={header.value}
    width={width}
  />
);

export function columnRender<T extends { widths: { [key: string]: number }}>(
  props: T,
  header: Header,
  type: ColumnType
): JSX.Element {
  const { widths} = props;
  return type === "data"
    ? dataColumn(props, header, widths[header.value] * 100)
    : actionColumn(props, header, 50);
}
