import { DraggableLocation } from "react-beautiful-dnd";
/**
 * thanks angular
 */
/**
 * Moves an item one index in an array to another.
 * @param array Array in which to move the item.
 * @param fromIndex Starting index of the item.
 * @param toIndex Index to which the item should be moved.
 */
function moveItemInArray<T = any>(
  array: T[],
  fromIndex: number,
  toIndex: number
): void {
  const from = clamp(fromIndex, array.length - 1);
  const to = clamp(toIndex, array.length - 1);
  if (from === to) {
    return;
  }
  const target = array[from];
  const delta = to < from ? -1 : 1;
  for (let i = from; i !== to; i += delta) {
    array[i] = array[i + delta];
  }

  array[to] = target;
}

/** Clamps a number between zero and a maximum. */
function clamp(value: number, max: number): number {
  return Math.max(0, Math.min(max, value));
}
/**
 * Moves an item from one array to another.
 * @param currentArray Array from which to transfer the item.
 * @param targetArray Array into which to put the item.
 * @param currentIndex Index of the item in its current array.
 * @param targetIndex Index at which to insert the item.
 */
function transferArrayItem<T = any>(
  currentArray: T[],
  targetArray: T[],
  currentIndex: number,
  targetIndex: number
): void {
  const from = clamp(currentIndex, currentArray.length - 1);
  const to = clamp(targetIndex, targetArray.length);

  if (currentArray.length) {
    targetArray.splice(to, 0, currentArray.splice(from, 1)[0]);
  }
}

/** end thanks angular */

// a little function to help us with reordering the result
export function reorder<T>(
  list: Array<T>,
  startIndex: number,
  endIndex: number
): Array<T> {
  const result = Array.from(list);
  moveItemInArray(result, startIndex, endIndex);
  return result;
}

//https://github.com/Microsoft/TypeScript/issues/24220
type MovePropsType = "selectedDroppable" | "unselectedDroppable";
type MoveResult<T> = { [key in MovePropsType]: Array<T> };
/**
 * Moves an item from one list to another list.
 */
export function move<T>(
  source: Array<T>,
  destination: Array<T>,
  droppableSource: DraggableLocation,
  droppableDestination: DraggableLocation
): MoveResult<T> {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  transferArrayItem(
    sourceClone,
    destClone,
    droppableSource.index,
    droppableDestination.index
  );

  return {
    [droppableSource.droppableId]: sourceClone,
    [droppableDestination.droppableId]: destClone
  } as MoveResult<T>;
}

/**
 * determine el destination between two list
 * assert that if the el is comming from the first list then dest is the second / is the other way around
 * @template T
 * @param {boolean} srcIsFirstList
 * @param {Array<T>} firstList
 * @param {Array<T>} secondList
 * @returns {{ src: Array<T>; dest: Array<T> }}
 */
export function setSimpleMoveDirection<T>(
  srcIsFirstList: boolean,
  firstList: Array<T>,
  secondList: Array<T>
): { src: Array<T>; dest: Array<T> } {
  const src = srcIsFirstList ? firstList : secondList;
  const dest = srcIsFirstList ? secondList : firstList;
  return {
    src,
    dest
  };
}
