import createBrowserHistory from "history/createBrowserHistory";
// programmatic nav
//https://stackoverflow.com/questions/39894547/navigating-programmatically-in-react-router-v4
export const history = createBrowserHistory();
