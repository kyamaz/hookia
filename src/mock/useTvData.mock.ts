import { FormatedListApi } from "./../model/api";
import { MOCK_LIST_DATA } from "@fixtures/list.data.fixtures";
import { displayData } from "@shared/utils";
import { MOCK_LIST_HEADERS } from "@fixtures/list.headers.fixtures";
import { TvItem } from "@model/tv";

export function mockSetData(): [FormatedListApi] {
  return [
    {
      headers: MOCK_LIST_HEADERS,
      allHeaders: MOCK_LIST_HEADERS,
      data: MOCK_LIST_DATA.map((d: TvItem) => ({
        key: Object.keys(d),
        data: d
      })).map((datum: any) => displayData(datum.key, datum.data))
    }
  ];
}
