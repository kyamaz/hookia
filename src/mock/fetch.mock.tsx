import * as React from "react";
import { LOGIN_ENDPOINT, TV_ENDPOINT } from "@shared/const";
import mockAxios from "axios";//jest spy axios 
import MockAdapter from 'axios-mock-adapter';
import { MOCK_LIST_DATA } from "../fixtures/list.data.fixtures";

export class Post extends React.Component<
  { url: string; axios: any , testId:string},
  { data: string }
> {
  static defaultProps = { mockAxios };
  state = { data: null };
  testId=this.props.testId
  componentDidUpdate(prevProps) {
    if (this.props.url !== prevProps.url) {
      this.fetch();
    }
  }
  fetch = async () => {
    const response = await this.props.axios.post(this.props.url);
    this.setState({ data: response.data });
  };
  render() {
    const { data } = this.state;
    return (
      <div>
        <button onClick={this.fetch}>Fetch</button>
        {data ? <span data-testid={this.testId}>{data[this.testId]}</span> : null}
      </div>
    );
  }
}
export class Fetch extends React.Component<
  { url: string; axios: any , testId:string},
  { data: string }
> {
  static defaultProps = { mockAxios };
  state = { data: null };
  testId=this.props.testId
  componentDidUpdate(prevProps) {
    if (this.props.url !== prevProps.url) {
      this.fetch();
    }
  }
  fetch = async () => {
    const response = await this.props.axios.get(this.props.url);
    this.setState({ data: response.data });
  };
  render() {
    const { data } = this.state;
    return (
      <div>
        <button onClick={this.fetch}>Fetch</button>
        {data ? <span data-testid={this.testId}>{data[this.testId]}</span> : null}
      </div>
    );
  }
}


const mock = new MockAdapter(mockAxios);

//success
mock.onPost(LOGIN_ENDPOINT,  { login: "valid", pwd: "valid" }).reply(200, {
  token: "newtoken"
});
export const mockPostValidLogin = async () => {
  try {
    const response = await mockAxios.post(LOGIN_ENDPOINT, { login: "valid", pwd: "valid" });
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message);
  }
};

//unauth creds
mock.onPost(LOGIN_ENDPOINT, {login: "invalid", pwd: "invalidinvalid" }).reply(401, {
  message:'Account Not Found'
});
export  const mockPostInvalidLogin = async () => {
  try {
    const response = await mockAxios.post(LOGIN_ENDPOINT,  {login: "invalid", pwd: "invalidinvalid" });
    return response.data;
  } catch (error) {
   throw new Error(error.response.data.message)
  }
};


//list 
mock.onGet(TV_ENDPOINT).reply(200, {
  status:200,
  data:MOCK_LIST_DATA
});
export  const mockSuccessTvData= async () => {
  try {
    const response = await mockAxios.get(TV_ENDPOINT);
    return response.data;

    
  } catch (error) {
   throw new Error(error.response.data.message)
  }
};