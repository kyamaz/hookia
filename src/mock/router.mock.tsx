import { MemoryRouter, Route, Router } from "react-router";
import * as React from 'react'
import { render, getByTestId } from "react-testing-library";
import { createMemoryHistory } from "history";
// this is a handy function that I would utilize for any component
// that relies on the router being in context

export function renderWithRouter(
    ui:JSX.Element,
    {route = '/', history = createMemoryHistory({initialEntries: [route]})} = {},
  ) :any{
    return {
      ...render(<Router history={history}>{ui}</Router>),
      // adding `history` to the returned utilities to allow us
      // to reference it in our tests (just try to avoid using
      // this to test implementation details).
      history,
    }
  }


  export function getTestIdWithRouter(component:JSX.Element, route:string, id:string):HTMLElement{
    const {getByTestId} = renderWithRouter(component, {route})
    return getByTestId(id)

  } 