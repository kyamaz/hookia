import * as React from "react";
import { AppContainer } from "@components/AppContainer/AppContainer";
import { Redirect } from "react-router";
import { AppToken } from "../../state/state";
import { Provider } from "@state/useStore";
import { props } from "bluebird";

const protectedRoutes = ({ component: Component, ...rest }): JSX.Element => {
  const { path, token } = rest;
  const storageToken = !!localStorage.getItem("hookia_token");
  const hasToken = !!token ? true : storageToken;
  const t = AppToken.get();
  if (!hasToken) {
    return <Redirect to="/login" />;
  }
  return (
    <Provider>
      <AppContainer exact path={path} component={Component}  />
    </Provider>
  );
};

export default protectedRoutes;
