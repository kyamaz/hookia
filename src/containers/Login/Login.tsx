import * as React from "react";
import { useState } from "react";
//style
import { DEFAULT_COLOR } from "@shared/style";
//components
import { LoginForm } from "@components/LoginForm/LoginForm";
import { VerticalColumns, LoginHeroBg, LoginTopBar } from "@ui/layout";
import { LoginSvg } from "@ui/loginSvg";
import { InlineBtn, SignInBtn } from "@ui/btn";
import { Tabs, TabItem } from "@ui/tabs";
//hooks
import { useToggle } from "@hooks/useToggle";
import { useTryAuth, useValidationError } from "@hooks/useTryLogin";
import { Modal } from "@components/Modal/Modal";
//Model
import {
  FormsControlsRef,
  LoginType,
  AuthPayload,
  UseLoginType,
  UseAuthPayload
} from "@model/index";

//Utils
import { formsCtrlsToPayload } from "@shared/utils/form";

interface LoginProps {}

function Login(props: LoginProps): JSX.Element {
  const [toggleForm, handleUiToogle] = useToggle(false);
  //state signin type
  const [loginType, setLoginType]: UseLoginType = useState<LoginType>("signin");
  //user try auth map
  const [tryAuthState, setTryAuthState]: UseAuthPayload = useState<AuthPayload>(
    null
  );
  const [tryAuth] = useTryAuth(tryAuthState);
  //reset validation error  signin type
  const [formTouched, setFormTouched] = useValidationError();

  const handleLoginType: (type: LoginType) => void = type => {
    handleResetValidationError();
    setLoginType(type);
  };
  const handlePane: (a: LoginType) => void = type => {
    handleUiToogle();
    handleLoginType(type);
  };
  const handleResetValidationError: () => void = () => setFormTouched(false);
  const handleTryAuth: (f: AuthPayload) => void = form => {
    setTryAuthState(form), setFormTouched(true);
  };
  return (
    <VerticalColumns>
      <LoginHeroBg isExpanded={toggleForm}>
        <LoginTopBar>
          <InlineBtn>
            <SignInBtn
              data-testid="signin-btn"
              id="signin-btn"
              onClick={() => handlePane("signin")}
            >
              sign in
            </SignInBtn>
            <SignInBtn
              data-testid="signup-btn"
              id="signup-btn"
              onClick={() => handlePane("signup")}
            >
              sign up
            </SignInBtn>
          </InlineBtn>
        </LoginTopBar>
        <Modal
          id="login-modal"
          isExpanded={toggleForm}
          onCloseModal={handleUiToogle}
          width="33%"
        >
          <div>
            <Tabs>
              <TabItem>
                <SignInBtn
                  id="signin-tab"
                  isActive={loginType === "signin"}
                  color={DEFAULT_COLOR}
                  onClick={() => handleLoginType("signin")}
                >
                  sign in
                </SignInBtn>
              </TabItem>
              <TabItem>
                <SignInBtn
                  id="signup-tab"
                  isActive={loginType === "signup"}
                  color={DEFAULT_COLOR}
                  onClick={() => handleLoginType("signup")}
                >
                  sign up
                </SignInBtn>
              </TabItem>
            </Tabs>
            <LoginForm
              id="login-form"
              loginType={loginType}
              tryAuth={handleTryAuth}
              resetValidationError={handleResetValidationError}
              validState={formTouched ? tryAuth.isSuccess : true}
            />
          </div>
        </Modal>
        <LoginSvg />
      </LoginHeroBg>
    </VerticalColumns>
  );
}
export default Login;
