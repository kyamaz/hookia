import * as React from "react";
import Tv from "./Tv";
import {
  cleanup,
  render,
  waitForElement,
  fireEvent
} from "react-testing-library";
import mockAxios from "@mock/axios";
import { mockSetData } from "@mock/useTvData.mock";
import { mockRenderer } from "@mock/useRerender.mock";
import { MOCK_LIST_DATA } from "@fixtures/list.data.fixtures";
import { Fetch } from "@mock/fetch.mock";
import { API, TV_ENDPOINT } from "@shared/const";

describe("Access to Tv page", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  beforeEach(() => {});

  afterEach(cleanup);
  test("It should render list on init", async () => {
    const { getByText, getByTestId } = render(
      <Fetch url={TV_ENDPOINT} axios={mockAxios} testId="data" />
    );
    fireEvent.click(getByText(/fetch/i));

    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(TV_ENDPOINT);

    const { container } = render(
      <Tv useDataHook={mockSetData} useRerenderHook={mockRenderer} />
    );
    const table = document.querySelector("#table-data");
    expect(!!table).toBe(true);
  });
});
