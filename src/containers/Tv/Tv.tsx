import * as React from "react";

import { AppTitle } from "@ui/title";
import { DataTable } from "@components/DataTable/DataTable";
import { TvItem } from "@model/tv";
import { connect } from "@state/useStore";
import { useData } from "@hooks/useTvData";
import { SET_LIST, SET_HEADERS, SET_WIDTHS } from "@state/actions";
import { useRerender } from "@hooks/useRerender";
import {
  ListWidths,
  ListHeaders,
  StoreState,
  DispatchToStore,
  ValidPayload,
  FormatedListApi,
  ViewFormatedData,
  Header
} from "@model/index";
import { ListWraper } from "@ui/layout";
import * as PropTypes from "prop-types";
import { useLatestValue } from "@hooks/useLatestValue";

interface TvComponentProps {
  headers: Array<Header>;
  data: Array<any>;
  widths: { [key: string]: number };
  setData: Function;
  setHeaders: Function;
  setWidths: Function;
  useDataHook?: Function;
  useRerenderHook?: Function;
}

function Tv(props: TvComponentProps): JSX.Element {
  const {
    setData,
    setHeaders,
    widths,
    setWidths,
    useDataHook,
    useRerenderHook,
    headers
  } = props;
  //use state to call rerender hook

  const onUseDataHook: Function = !!useDataHook ? useDataHook : useData;
  const onRerenderHook: Function = !!useRerenderHook
    ? useRerenderHook
    : useRerender;

  const [query, setQuery]: [string, Function] = React.useState<string>(
    "page=1"
  );
  const [apiData]: [FormatedListApi] = onUseDataHook.call(this, query);
  const [updatedData]: [Array<ViewFormatedData>] = onRerenderHook(
    apiData.data,
    setData.bind(this)
  );

  //update store with api data
  const []: [] = onRerenderHook(apiData.headers, setHeaders.bind(this));
  const [useLatestHeader] = useLatestValue<Array<Header>>(headers, []);
  const [newWidths, setNewWidths]: [ListWidths, Function] = React.useState(
    widths
  );
  const [updatedWidths]: [ListWidths] = onRerenderHook(
    newWidths,
    setWidths.bind(this)
  );
  const handleSetWidths = (changedWidths: ListWidths): void =>
    setNewWidths(changedWidths);
  return (
    <ListWraper>
      <AppTitle data-testid="list-title">Dynamic list sample</AppTitle>
      
        <DataTable
          tableData={updatedData}
          headers={useLatestHeader}
          widths={updatedWidths}
          onHandleSetWidths={handleSetWidths}
          allHeaders={apiData.allHeaders}
        />
    
    </ListWraper>
  );
}
const mapStateToProps = (state: StoreState, props: TvComponentProps) => ({
  data: state.list.data,
  headers: state.list.headers,
  widths: state.list.widths
});

const mapDispatchToProps = (dispatch: DispatchToStore<ValidPayload>) => ({
  setData: (payload: Array<TvItem>) => dispatch({ type: SET_LIST, payload }),
  setHeaders: (payload: ListHeaders) =>
    dispatch({ type: SET_HEADERS, payload }),
  setWidths: (payload: ListWidths) => dispatch({ type: SET_WIDTHS, payload })
});

const ConnectedTv = connect(
  mapStateToProps,
  mapDispatchToProps
)(Tv);

ConnectedTv.propTypes = {
  useDataHook: PropTypes.func,
  useRerenderHook: PropTypes.func
};

export default ConnectedTv;
