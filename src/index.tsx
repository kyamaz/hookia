import * as React from "react";
import * as ReactDOM from "react-dom";
import { Fragment } from "react";
import registerServiceWorker from "./registerServiceWorker";
//css library
 import "spectre.css/dist/spectre.min.css";
import "spectre.css/dist/spectre-icons.css"; 

import 'react-virtualized/styles.css'; // only needs to be imported once
import 'antd/dist/antd.css'; 
import "./styles.css";
//style
import { GlobalStyle } from "@shared/style";
//navigation
import { Route, Switch, Router } from "react-router-dom";
import { history } from "./route-history";

//routes
import { AppContainer, LoginContainer, TvContainer } from "./routes";
import ProtectedRoute from "./containers/ProtectedRoute/ProtectedRoute";

function AppWrap(): JSX.Element {
  return (
    <Router history={history}>
      <Fragment>
        <GlobalStyle />
        <Switch>
          <Route
            path="/login"
            render={props => <LoginContainer {...props} />}
          />
          <ProtectedRoute path="/" exact component={AppContainer} />
          <ProtectedRoute path="/list" exact  component={TvContainer} />

          <ProtectedRoute component={AppContainer} />
        </Switch>
      </Fragment>
    </Router>
  );
}

const IndexComponent: JSX.Element = AppWrap();

ReactDOM.render(IndexComponent, document.getElementById("root") as HTMLElement);
registerServiceWorker();

