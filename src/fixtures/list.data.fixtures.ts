import { TvItem } from "@model/tv";
export const MOCK_LIST_DATA: Array<TvItem> = [
  {
    id: 9031,
    url: "http://www.tvmaze.com/episodes/9031/house-1x01-pilot",
    name: "Pilot",
    season: 1,
    number: 1,
    airdate: "2004-11-16",
    airtime: "21:00",
    airstamp: "2004-11-17T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144510.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144510.jpg"
    },
    summary:
      "<p>Dr, Gregory House and his team take medical cases only after everyone else has failed to diagnose a patient. When a kindergarten teacher passes out in front of her class, House and his team use trial-and-error to figure out why she is slowly dying.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9031" } }
  },
  {
    id: 9032,
    url: "http://www.tvmaze.com/episodes/9032/house-1x02-paternity",
    name: "Paternity",
    season: 1,
    number: 2,
    airdate: "2004-11-23",
    airtime: "21:00",
    airstamp: "2004-11-24T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144511.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144511.jpg"
    },
    summary:
      "<p>When a teenage lacrosse player is stricken with an unidentifiable brain disease, Dr. House and the team hustle to give his parents answers.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9032" } }
  },
  {
    id: 9033,
    url: "http://www.tvmaze.com/episodes/9033/house-1x03-occams-razor",
    name: "Occam's Razor",
    season: 1,
    number: 3,
    airdate: "2004-11-30",
    airtime: "21:00",
    airstamp: "2004-12-01T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144512.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144512.jpg"
    },
    summary:
      "<p>When a college student collapses after a bout of raucous sex with his girlfriend, Dr. House and his team scramble to figure out why.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9033" } }
  },
  {
    id: 9034,
    url: "http://www.tvmaze.com/episodes/9034/house-1x04-maternity",
    name: "Maternity",
    season: 1,
    number: 4,
    airdate: "2004-12-07",
    airtime: "21:00",
    airstamp: "2004-12-08T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144513.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144513.jpg"
    },
    summary:
      "<p>Dr. House exasperates his boss, Dr. Lisa Cuddy, when he suggests that two sick newborn babies in one hospital add up to an epidemic. Even more frightening is the fact he may be right.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9034" } }
  },
  {
    id: 9035,
    url: "http://www.tvmaze.com/episodes/9035/house-1x05-damned-if-you-do",
    name: "Damned If You Do",
    season: 1,
    number: 5,
    airdate: "2004-12-14",
    airtime: "21:00",
    airstamp: "2004-12-15T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144514.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144514.jpg"
    },
    summary:
      "<p>When a nun comes into the clinic with swollen arms, rash and bleeding in her palms, Dr. House's diagnosis is a bad allergy, not stigmata. However, when he gives her a shot of medication, she has a heart attack, and whether he gave her the correct amount of medicine comes into question.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9035" } }
  },
  {
    id: 9036,
    url: "http://www.tvmaze.com/episodes/9036/house-1x06-the-socratic-method",
    name: "The Socratic Method",
    season: 1,
    number: 6,
    airdate: "2004-12-21",
    airtime: "21:00",
    airstamp: "2004-12-22T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144515.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144515.jpg"
    },
    summary:
      "<p>When it appears that Lucy Palermo, a schizophrenic mom with deadly deep vein thrombosis, is lying about her alcohol intake, Dr. House is the lone voice of reason.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9036" } }
  },
  {
    id: 9037,
    url: "http://www.tvmaze.com/episodes/9037/house-1x07-fidelity",
    name: "Fidelity",
    season: 1,
    number: 7,
    airdate: "2004-12-28",
    airtime: "21:00",
    airstamp: "2004-12-29T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144516.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144516.jpg"
    },
    summary:
      "<p>When young wife Elyse falls ill with a rare disease she can't possibly have, Dr. Cameron is very interested in the case - so much so that her interest convinces House to take the case, and the team struggles to save her and appease her very attentive husband.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9037" } }
  },
  {
    id: 9038,
    url: "http://www.tvmaze.com/episodes/9038/house-1x08-poison",
    name: "Poison",
    season: 1,
    number: 8,
    airdate: "2005-01-25",
    airtime: "21:00",
    airstamp: "2005-01-26T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144517.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144517.jpg"
    },
    summary:
      "<p>When a high school student falls victim to a mysterious but lethal poisoning, House and his team jump in to find out what is killing the teen.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9038" } }
  },
  {
    id: 9039,
    url: "http://www.tvmaze.com/episodes/9039/house-1x09-dnr",
    name: "DNR",
    season: 1,
    number: 9,
    airdate: "2005-02-01",
    airtime: "21:00",
    airstamp: "2005-02-02T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144518.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144518.jpg"
    },
    summary:
      "<p>Legendary jazz musician John Henry Giles checks into Princeton Plainsboro Teaching Hospital believing he's dying from ALS and signs a DNR to avoid a slow death. House disagrees with the diagnosis and goes against everyone's wishes when he violates the DNR to save Giles' life.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9039" } }
  },
  {
    id: 9040,
    url: "http://www.tvmaze.com/episodes/9040/house-1x10-histories",
    name: "Histories",
    season: 1,
    number: 10,
    airdate: "2005-02-08",
    airtime: "21:00",
    airstamp: "2005-02-09T02:00:00+00:00",
    runtime: 60,
    image: {
      medium:
        "http://static.tvmaze.com/uploads/images/medium_landscape/57/144519.jpg",
      original:
        "http://static.tvmaze.com/uploads/images/original_untouched/57/144519.jpg"
    },
    summary:
      "<p>Dr. Foreman believes an uncooperative homeless woman is faking seizures to get a meal ticket at the teaching hospital. But her homelessness strikes a personal chord with Dr. Wilson and he grows determined to keep her from falling between the cracks.</p>",
    _links: { self: { href: "http://api.tvmaze.com/episodes/9040" } }
  }
];
