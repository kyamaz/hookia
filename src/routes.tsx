import * as React from "react";
import Loadable from "react-loadable";

//lazy loaded routes
function Loading({ error }) {
  if (error) {
    console.error(error);
    return "Oh nooess!";
  } else {
    return <div />;
  }
}

//important side note
//loadable loading expects component. not just plain jsx
//component must have default export.not nammed export

export const AppContainer = Loadable({
  loader: () => import("./containers/App/App"),
  loading: Loading
});

export const TvContainer = Loadable({
  loader: () => import("./containers/Tv/Tv"),
  loading: Loading
});

//login
export const LoginContainer = Loadable({
  loader: () => import("./containers/Login/Login"),
  loading: Loading
});
