import { ListHeaders } from "@model/index";
export type ApiAuthResponse = ApiAuthSuccess | ApiAuthFail;
export type ApiAuthSuccess = { token: string };
export type ApiAuthFail = { message: string };

export type BaseApiResponse<T> = T;

export interface ApiInterface<T extends unknown> {
  status: number;
  data: T;
}
export interface FormatedListApi {
  data: Array<ViewFormatedData>;
  headers: ListHeaders;
  allHeaders: ListHeaders;
}

export interface ViewFormatedData {
  [key: string]: JSX.Element;
}
