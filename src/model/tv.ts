import { Header } from "@model/index";
export interface TvItem {
  airdate: string;
  airstamp: string;
  airtime: string;
  id: number;
  image: TvImage;
  name: string;
  number: number;
  runtime: number;
  season: number;
  summary: string;
  url: string;
  _links: { self: { href: string } };
}

export interface TvImage {
  medium: string;
  original: string;
}
export interface ApisData<T> {
  headers: Array<Header>;
  data: Array<T>;
}
