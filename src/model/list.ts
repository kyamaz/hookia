import { TvItem } from "./tv";

export interface ListWidths {
  [key: string]: number;
}
export interface Header {
  value: string;
  viewValue: string;
  isSelected: boolean;
}
export type ListHeaders = Array<Header>;
export interface StoreListState {
  widths: ListWidths;
  data: Array<TvItem>;
  headers: ListHeaders;
}

export type ColumnType = "data" | "action";
