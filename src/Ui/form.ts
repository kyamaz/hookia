import { ERROR_COLOR } from "./../shared/style";
import styled from "styled-components";

export const HorizontalForm = styled.form.attrs({
  className: "form-horizontal"
})`
  min-height: 75vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
`;
export const FormGroup = styled.div.attrs({
  className: "form-group"
})<{ isValid: boolean }>`
  margin: 10px 0;
  .form-input {
    border-color: ${props => (props.isValid ? "#bcc3ce" : ERROR_COLOR)};
  }
`;
export const FormLabel = styled.label.attrs({
  className: "form-label"
})<{ htmlFor: string }>``;

export const FormHint = styled.p`
  color: ${ERROR_COLOR};
  font-size: 0.7rem;
  padding-top: 0.2rem;
  margin-bottom: 0;
  min-height: 20px;
  width: 100%;
`;
