import * as React from "react";
import styled from "styled-components";
import { inherits } from "util";

interface StyledBtnProps {
  isFormCta?: boolean;
}
interface StyledGroupBtnProps {
  isFormCta?: boolean;
}
export const PrimaryBtn = styled.button.attrs({ className: "btn btn-primary" })<
  StyledBtnProps
>`
  margin: 0 10px;
  border-radius: 5px;
  width: ${props => (props.isFormCta ? "100%" : "initial")};
  margin-top: ${props => (props.isFormCta ? "20px" : "inherit")};
  height: ${props => (props.isFormCta ? "40px" : "initial")};
`;

export const StickyGroupBtn = styled.div<StyledGroupBtnProps>`
  display: inline-flex;
  height: 5rem;
  justify-content: flex-end;
  align-items: center;
  position: sticky;
  top: 2rem;
  z-index: 999;
  margin-right: 6rem;
`;

export const InlineBtn = styled.div`
  display: inline-flex;
`;

//base inline
interface LinkBtnProps {
  isActive?: boolean;
}
const LinkBtn = styled.button.attrs({
  className: "btn login"
})<LinkBtnProps>`
  background: 0 0;
  border-color: transparent;
  &:hover,
  &:focus {
    background: transparent;
  }
  color: white;
  font-weight: ${props => (props.isActive ? 600 : 400)};
`;

//exported inline btn style
export const SignInBtn = styled(LinkBtn)`
  color: ${props => props.color};
`;
