import styled from "styled-components";
export const MenuItem = styled.li.attrs({
  className: "menu-item"
})<{}>``;
export const MenuLabel = styled.span<{}>`
  text-align: center;
`;
export const MenuValue = styled.span<{}>``;
