import styled from "styled-components";
export const RenderLoading = styled.div.attrs({
  className: "loading loading-lg"
})`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #f7f8f9;
  height: 100vh;
  z-index: 99;
`;
