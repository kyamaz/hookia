import * as React from "react";
import { render, fireEvent, cleanup, getByTestId } from "react-testing-library";

import { getTestIdWithRouter } from "@mock/router.mock";
import { DraggableItem } from "./DraggableItem";
import { MOCK_LIST_HEADERS } from "@fixtures/list.headers.fixtures";
import { INIT_WIDTHS } from "@state/const";
import { ID_VHEADER } from "@fixtures/virtual-header.fixtures";
describe("nav in app", () => {

  beforeEach(function () {
    // import and pass your custom axios instance to this method
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    cleanup()
  })

  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);
  test("It should links to all page", () => {
    const { getByTestId } = render(
        <DraggableItem
        vHeader= {ID_VHEADER}
        headers= {MOCK_LIST_HEADERS}
        handleSetWidths= {()=>null}
        widths= {INIT_WIDTHS}
        /> 
  );
    })
});