import * as React from "react";
import Draggable, { DraggableData } from "react-draggable";
import { TableHeaderProps } from "react-virtualized";
import * as PropTypes from "prop-types";
import { ListWidths, Header } from "@model/index";
import { DragHandle } from "@ui/icon";
import { SpaceBetween } from "@ui/layout";
import { DraggLabel } from "@ui/text";

export function resizeRow({ dataKey, deltaX }, state, headers): ListWidths {
  const prevWidths = { ...state };
  const percentDelta = deltaX / 150;
  const keyPos = headers.findIndex(h => h === dataKey);
  const nextDataKey = headers[keyPos + 1];
  return {
    ...prevWidths,
    [dataKey]: prevWidths[dataKey] + percentDelta,
    [nextDataKey]: prevWidths[nextDataKey] - percentDelta
  };
}

function updateWidths(
  event:
    | React.MouseEvent<HTMLElement | SVGElement>
    | React.TouchEvent<HTMLElement | SVGElement>
    | MouseEvent
    | TouchEvent,
  { deltaX }: Partial<DraggableData>,
  dataKey: string,
  props: Props
): ListWidths {
  const { headers, widths } = props;
  return resizeRow(
    {
      dataKey,
      deltaX
    },
    widths,
    headers
  );
}
interface Props {
  vHeader: TableHeaderProps;
  headers: Array<Header>;
  handleSetWidths?: Function;
  widths: { [key: string]: number };
}
function draggableItem(props: Props): JSX.Element {
  const { vHeader, headers, handleSetWidths, widths } = props;
  const {
    columnData,
    dataKey,
    disableSort,
    label,
    sortBy,
    sortDirection
  } = vHeader;

  return (
    <SpaceBetween
      key={dataKey}
      data-testid={`table-header-${dataKey}`}
    >
      <DraggLabel className="ReactVirtualized__Table__headerTruncatedText">
        {label}
      </DraggLabel>
      <Draggable
        axis="x"
        onDrag={(ev, { deltaX }) =>
          handleSetWidths(updateWidths(ev, { deltaX }, dataKey, props))
        }
        data-testid="draggable-element"

      >
      <DragHandle />
      </Draggable>
    </SpaceBetween>
  );
}

draggableItem.propTypes = {
  handleSetWidths: PropTypes.func
};

export { draggableItem as DraggableItem };
