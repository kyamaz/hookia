import * as React from "react";

import {
  Header,
  DispatchToStore,
  ValidPayload,
  ListHeaders,
  StoreState
} from "@model/index";
import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";
import { DraggableList } from "@components/DraggableList/DraggableList";
import {
  reorder,
  move,
  setSimpleMoveDirection,
} from "@shared/utils";
import { makeHeadersList } from "@shared/utils/list/header";
import { useSimpleDraggableList } from "@hooks/useDraggableList";
import { MenuDivider } from "@ui/divider";
import { SET_HEADERS } from "@state/actions";
import { connect } from "@state/useStore";
import { useDispachToProps } from "@hooks/useDispatchToProps";
import { ColumnListTitle, TableDDContainer } from "@ui/list";
const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? '#fc80cb' : 'lightgrey'});



interface DraggableHeadersProps {
  selectedList: Array<Header>;
  fullList: Array<Header>;
  setHeaders: Function;
  headers: Array<Header>; //store headers state
}
function updatedSelectList(payload: Array<Header>) {
  return payload.map(h => ({ ...h, isSelected: true }));
}
function draggableHeaders(props: DraggableHeadersProps): JSX.Element {
  const { selectedList, fullList, setHeaders } = props;
  const list = makeHeadersList(selectedList, fullList);
  const [
    useSelected,
    setUseSelected,
    useUnSelected,
    setUseUnSelected
  ] = useSimpleDraggableList([list.isSelected, list.isNotSelected]);

  //updated list is dispatched to store
  const [] = useDispachToProps<Array<Header>>(
    useSelected,
    setHeaders,
    updatedSelectList
  );

  const onDragEnd = (result: DropResult): void => {
    const { source, destination } = result;
    // dropped outside the list
    if (!destination) {
      return;
    }
    const isElFromFirstList: boolean =
      source.droppableId === "droppableSelected";

    if (source.droppableId === destination.droppableId) {
      const listToUpdate: Array<Header> = isElFromFirstList
        ? useSelected
        : useUnSelected;
      const updatedList: Array<Header> = reorder(
        listToUpdate,
        source.index,
        destination.index
      );
      if (isElFromFirstList) {
        return setUseSelected(updatedList);
      }
      return setUseUnSelected(updatedList);
    } else {
      const { src, dest } = setSimpleMoveDirection<Header>(
        isElFromFirstList,
        useSelected,
        useUnSelected
      );
      const result = move(src, dest, source, destination);
      const { droppableSelected, droppableUnSelected } = result as any;
      setUseSelected(droppableSelected);
      setUseUnSelected(droppableUnSelected);
    }
  };

  return (
    <TableDDContainer>
      <DragDropContext onDragEnd={onDragEnd}>
        <ColumnListTitle>displayed column</ColumnListTitle>
        <Droppable droppableId="droppableSelected">
          {(provided, snapshot) => (
            <DraggableList {...provided} list={useSelected}    style={getListStyle(snapshot.isDraggingOver)} />
          )}
        </Droppable>
        <MenuDivider />
        <ColumnListTitle>hidden column</ColumnListTitle>
        <Droppable droppableId="droppableUnSelected">
          {(provided, snapshot) => (
            <DraggableList {...provided} list={useUnSelected}  style={getListStyle(snapshot.isDraggingOver)} />
          )}
        </Droppable>
      </DragDropContext>
    </TableDDContainer>
  );
}

const mapDispatchToProps = (dispatch: DispatchToStore<ValidPayload>) => ({
  setHeaders: (payload: ListHeaders) => dispatch({ type: SET_HEADERS, payload })
});

const ConnectedDraggableHeaders = connect(
  () => {},
  mapDispatchToProps
)(draggableHeaders);

export { ConnectedDraggableHeaders as DraggableHeaders };
