import * as React from "react";
import styled from "styled-components";
import * as PropTypes from "prop-types";
import { useRef } from "react";
const Input = styled.input.attrs({ className: "form-input" })``;
interface LoginFormProps {
  id: "login-input" | "pwd-input" | "confirm-input";
  value: string | number;
  detail: { name: string; type: string; placeholder: string };
  handleUpdate: Function;
  name?:"login"| "pwd"
}
function formInput(props: LoginFormProps): JSX.Element {
  const {
    detail: { name, type, placeholder },
    value
  } = props;
  const inputEl = useRef(null);

  return (
    <Input
      id={props.id}
      type={type}
      ref={inputEl}
      value={value}
      data-testid={props.id}
      onChange={(ev: React.ChangeEvent<HTMLInputElement>) => {
        props.handleUpdate({ name, value: ev.target.value });
        inputEl.current.focus();
      }}
      placeholder={placeholder}
    />
  );
}
formInput.propTypes = {
  handleUpdate: PropTypes.func
};
export { formInput as FormInput };
