import * as React from "react";
import { DroppableProvided, Draggable } from "react-beautiful-dnd";


const getItemStyle = (isDragging:boolean, draggableStyle:Object):Object => ({
  // change background colour if dragging
  background:'white',
  boxShadow:isDragging?"0 6px 9px rgba(0, 0, 0, 0.16), 0 6px 9px rgba(0, 0, 0, 0.23)":null,
  padding: "4px 0",
  color:"initial",
  fontWeight:"400",
  // styles we need to apply on draggables
  ...draggableStyle
});

interface DraggableListProps<T> extends DroppableProvided {
  list: Array<T>;
  style:any
}
function draggableList<T extends { value: string; viewValue: string }>(
  props: DraggableListProps<T>
): JSX.Element {
  const { innerRef, list, placeholder } = props;
  return (
    <div ref={innerRef} style={props.style}>
      {list.map((item, index) => (
        <Draggable key={item.value} draggableId={item.value} index={index}
        >
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              style={getItemStyle(
                snapshot.isDragging,
                provided.draggableProps.style
            )}
            >
              {item.viewValue}
            </div>
          )}
        </Draggable>
      ))}
      {placeholder}
    </div>
  );
}

export { draggableList as DraggableList };
