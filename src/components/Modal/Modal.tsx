import * as React from "react";

import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";

import { useModalInit } from "@hooks/useModalInit";
import * as PropTypes from "prop-types";

interface ModalProps {
  isExpanded?: boolean;
  onCloseModal?: Function;
  from?: 'left'| 'bottom' | 'right';
  width?:string;
  children?: React.ReactNode, 
  id:"login-modal"
}

function modal(props: ModalProps):JSX.Element {
  const togglePane = (e: React.SyntheticEvent) => {
    e.preventDefault();
    props.onCloseModal(false);
  };

  const modal = React.useRef(null);
  useModalInit(modal);
  const { from, isExpanded, width } = props;

  return (
    <div ref={modal}>
      <SlidingPane
        isOpen={isExpanded}
        onRequestClose={togglePane}
        from={from}
        width={width}
        overlayClassName="zIndex"
      >
        {props.children}
      </SlidingPane>
    </div>
  );
}
modal.propTypes = {
  onCloseModal: PropTypes.func
};

export { modal as Modal };
