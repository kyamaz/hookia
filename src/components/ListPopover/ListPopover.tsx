import * as React from "react";
import { Popover } from "antd";
interface ListPopoverProps {
  content: JSX.Element;
  dataKey: string;
  trigger: JSX.Element;
  url?: string;
}

function listPopover(props: ListPopoverProps): any {
  const { content, trigger } = props;
  return (
    <React.Fragment>
      <Popover
        overlayStyle={{ maxWidth: "250px" }}
        placement="bottom"
        content={content}
        trigger="hover"
      >
        {trigger}
      </Popover>
    </React.Fragment>
  );
}

export { listPopover as ListPopover };
