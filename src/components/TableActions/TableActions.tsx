import * as React from "react";
import { TableHeaderProps } from "react-virtualized";
import { Popover, Button } from "antd";
import { DraggableHeaders } from "@components/DraggableHeaders/DraggableHeaders";
import { Header } from "@model/index";

const content = (
  <div>
    <p>Content</p>
    <p>Content</p>
  </div>
);

interface Props {
  vHeader: TableHeaderProps;
  headers: Array<Header>;
  allHeaders: Array<Header>;

}
function tableActions(props: Props): JSX.Element {
  const {
    vHeader: { columnData, dataKey },
    headers,
    allHeaders
  } = props;
  return (
    <React.Fragment>
      <Popover
        overlayStyle={{ width:"210px" }}
        content={<DraggableHeaders selectedList={headers} fullList={allHeaders} />}
        placement="bottom"
        trigger="click"
      >
        <span
          data-testid="table-action"
          className="icon icon-apps icon--table-action"
        />
      </Popover>
    </React.Fragment>
  );
}

export { tableActions as TableActions };
