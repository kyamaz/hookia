import * as React from "react";
import { HorizontalForm, FormHint } from "@ui/form";
import { FormInput } from "@components/FormInput/FormInput";
import { FormUpdatePayload } from "@model/index";
import { useLoginForm } from "@hooks/useLoginForm";
import { FormGroupLabeled } from "@components/FormGroupLabeled/FormGroupLabeled";
import { Columns } from "@ui/layout";
import { PrimaryBtn } from "@ui/btn";
import * as PropTypes from "prop-types";
import { uiValidation, displayErrorHint, formsCtrlsToPayload } from "@shared/utils/form";

interface LoginFormProps {
  loginType: string;
  tryAuth: Function;
  resetValidationError: Function;
  validState: boolean;
  id:"login-form"
}

function loginForm(props: LoginFormProps): JSX.Element {
  const isSignup: boolean = props.loginType === "signup";
  const {
    loginInput,
    pwdInput,
    confirmInput,
    isFormValid,
    inputsRef,
    updateFormState
  } = useLoginForm(isSignup);
  const onHandleUpdate = (f: FormUpdatePayload): void => {
    updateFormState(f, inputsRef);
    props.resetValidationError();
  };
  const _confirmLabel: JSX.Element = (
    <FormGroupLabeled
      label="confirm"
      for="confirm-input"
      isValid={uiValidation(inputsRef.confirmInput)}
    >
      <FormInput
        id="confirm-input"
        detail={{
          name: "confirmInput",
          type: "password",
          placeholder: "confirm password"
        }}
        value={confirmInput.value}
        handleUpdate={onHandleUpdate}
      />
      <FormHint>{displayErrorHint(inputsRef.confirmInput)}</FormHint>
    </FormGroupLabeled>
  );
  const confirm: JSX.Element = isSignup ? _confirmLabel : null;

  return (
    <HorizontalForm
      data-testid="login-form"
      onSubmit={event => {
        event.preventDefault();
        const payload = formsCtrlsToPayload(inputsRef);
        props.tryAuth(payload);
      }}
    >
      <FormGroupLabeled
        label="login"
        for="login-input"
        data-testid='login-formgroup'
        isValid={uiValidation(inputsRef.loginInput)}
      >
        <FormInput
          id="login-input"
          detail={{ name: "loginInput", type: "text", placeholder: "login" }}
          value={loginInput.value}
          handleUpdate={onHandleUpdate}
          name="login"
        />
        <FormHint  data-testid='input-error'>{displayErrorHint(inputsRef.loginInput)}</FormHint>
      </FormGroupLabeled>
      <FormGroupLabeled
        label="password"
        for="pwd-input"
        isValid={uiValidation(inputsRef.pwdInput)}
      >
        <FormInput
          id="pwd-input"
          detail={{
            name: "pwdInput",
            type: "password",
            placeholder: "password"
          }}
          value={pwdInput.value}
          handleUpdate={onHandleUpdate}
          name="pwd"

        />
        <FormHint data-testid='pwd-error'>{displayErrorHint(inputsRef.pwdInput)}</FormHint>
      </FormGroupLabeled>
      {confirm}
      <Columns>
        <PrimaryBtn
          isFormCta={true}
          disabled={!isFormValid}
          type="submit"
          id="login-submit"
          data-testid="login-submit"
        >
          submit
        </PrimaryBtn>
        <FormHint id='form-hint'>{props.validState ? null : "login failed"} </FormHint>
      </Columns>
    </HorizontalForm>
  );
}
loginForm.propTypes = {
  tryAuth: PropTypes.func,
  resetValidationError: PropTypes.func
};

export { loginForm as LoginForm };
