import * as React from "react";

import { MOCK_LIST_HEADERS } from "@fixtures/list.headers.fixtures";

import { cleanup, render } from "react-testing-library";
import { DataTable } from "./DataTable";
import { INIT_WIDTHS } from "@state/const";
import { mockSetData } from "@mock/useTvData.mock";
import { Header } from "@model/index";

function mockHandle(data) {
  data;
}
describe("data table", () => {
  beforeEach(function() {
    // import and pass your custom axios instance to this method
  });

  afterEach(function() {
    // import and pass your custom axios instance to this method
    cleanup();
  });

  test("It should display headers name", () => {
    const { getByTestId, container, queryByTestId } = render(
      <DataTable
        headers={MOCK_LIST_HEADERS}
        allHeaders={MOCK_LIST_HEADERS}
        tableData={mockSetData()}
        onHandleSetWidths={mockHandle}
        widths={INIT_WIDTHS}
      />
    );
    MOCK_LIST_HEADERS.map(
      (header: Header) =>
        expect(queryByTestId(`table-header-${header.value}`).textContent).toBeInTheDOM
    );
  });
  test("It should display headers", () => {
    const { getByTestId, container, queryByTestId } = render(
      <DataTable
        headers={MOCK_LIST_HEADERS}
        allHeaders={MOCK_LIST_HEADERS}
        tableData={mockSetData()}
        onHandleSetWidths={mockHandle}
        widths={INIT_WIDTHS}
      />
    );
    //assert
    const countHeader = document.querySelectorAll("div[role='columnheader']*").length;
    expect(countHeader).toBe(MOCK_LIST_HEADERS.length+1 );
  });


  test("It should display data in rows", () => {
    const { getByTestId, container, queryByTestId } = render(
      <DataTable
        allHeaders={MOCK_LIST_HEADERS}
        headers={MOCK_LIST_HEADERS}
        tableData={mockSetData()}
        onHandleSetWidths={mockHandle}
        widths={INIT_WIDTHS}
      />
    );

    //assert
    //min visible row
    const { data }=mockSetData()[0]
    const countRow = document.querySelectorAll("div[role='row'] *").length;
    expect(countRow).toBeGreaterThanOrEqual(data.length);
  });
});
