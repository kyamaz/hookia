import * as React from "react";
import { AutoSizer, Table } from "react-virtualized";
import * as PropTypes from "prop-types";
import {
  rowRender,
  columnRender
} from "@shared/utils/index";
import { SortableContainer } from "react-sortable-hoc";
import { Header } from "@model/index";

export interface DataTableProps<T> {
  headers: Array<Header>;
  allHeaders:Array<Header>
  tableData: Array<T>;
  widths: { [key: string]: number };
  onHandleSetWidths: Function;
}
const SortableTable = SortableContainer(Table as any) as any;

function dataTable<T extends unknown>(props: DataTableProps<T>): JSX.Element {
  const { headers, tableData, widths, onHandleSetWidths } = props;
  return (
    
    <AutoSizer>
      {({ height, width }) => (
        <SortableTable
          width={width}
          height={height}
          defaultHeight={500}
          headerHeight={50}
          rowHeight={65}
          rowCount={tableData.length}
          rowGetter={({ index }) => tableData[index]}
          className="hookia__table"
          headerClassName="hookia__header"
          rowClassName="hookia__row"
          id="table-data"
          gridClassName="hookia__grid"
          rowRenderer={rowRender}
        >
          {headers.map((header: Header, idx: number) =>
            columnRender<DataTableProps<T>>(props, header, "data")
          )}
          {columnRender<DataTableProps<T>>({ ...props }, { viewValue:"gear", value:"gear", isSelected:true}, "action")}
        </SortableTable>
      )}
    </AutoSizer>
  );
}
dataTable.propTypes = {
  onHandleSetWidths: PropTypes.func
};

export { dataTable as DataTable };
