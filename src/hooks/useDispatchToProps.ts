import * as React from "react";

export function useDispachToProps<T>(
  payload: T,
  dispatcher: Function,
  formatData?: Function
): [] {
  React.useEffect(() => {
    if (Array.isArray(payload) && payload.length === 0) {
      return;
    }
    const data = formatData ? formatData(payload) : payload;
    dispatcher(data);
  }, [payload]);

  return [];
}
