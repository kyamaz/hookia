import { useState, SetStateAction, Dispatch } from "react";

export const useToggle = (
  initState: boolean
): [boolean, (b?: boolean) => void] => {
  const [toggleState, setToggleState] = useState(initState);
  const handleUiToogle: (b?: boolean) => void = (b = !toggleState) =>
    setToggleState(b);
  return [toggleState, handleUiToogle];
};
