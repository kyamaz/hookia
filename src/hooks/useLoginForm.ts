import { controlsAreValid, formatInputState } from "./../shared/utils/form";
import { FormsControlsRef, FormUpdatePayload } from "./../model/form";
import { InputState, UseHooks, UseBoolean, UseLoginHook } from "@model/index";
import { useState, useEffect } from "react";
import { makeFormState, validate } from "@shared/utils/form";

const loginInitialState: InputState = formatInputState(["required"]);
const pwInitialState: InputState = formatInputState(["required", "minLength"]);
const confirmInitialState: InputState = formatInputState([]);

function updateFormState(
  f: FormUpdatePayload,
  formState: FormsControlsRef
): void {
  const { value, name } = f;
  if (formState.hasOwnProperty(name)) {
    const stateValidation = formState[name].validations;
    const stateSetter = formState[name].setter;
    const validationState = validate(value, stateValidation);
    const updatedF = {
      value,
      touched: true,
      valid: validationState.value,
      errorDisplay: !validationState.value ? validationState.message : null
    };
    stateSetter(updatedF);
  }
}

export function useLoginForm(isSignUp: boolean): UseLoginHook {
  //state
  const [loginInput, setLoginInput]: UseHooks<InputState> = useState(
    loginInitialState
  );
  const [pwdInput, setPwdInput]: UseHooks<InputState> = useState(
    pwInitialState
  );
  const [confirmInput, setConfirmInput]: UseHooks<InputState> = useState(
    pwInitialState
  );
  const [isFormValid, setIsFormValid]: UseBoolean = useState(false);
  //global state of form && access to use state method
  const inputsRef: FormsControlsRef = makeFormState([
    ["loginInput", loginInput, setLoginInput, loginInitialState.validationType],
    ["pwdInput", pwdInput, setPwdInput, pwInitialState.validationType],
    [
      "confirmInput",
      confirmInput,
      setConfirmInput,
      confirmInitialState.validationType
    ]
  ]);
  const fieldsToValidate = isSignUp
    ? { loginInput, pwdInput, confirmInput }
    : { loginInput, pwdInput };
  useEffect(
    () => {
      //in siginin mode, return true as don't need to test the conditoin
      const testIsEqual = isSignUp
        ? pwdInput.value === confirmInput.value
        : true;

      inputsRef.confirmInput.state.errorDisplay = testIsEqual
        ? null
        : "isEqual";
      const formValidationState =
        controlsAreValid(fieldsToValidate) && testIsEqual;
      setIsFormValid(formValidationState);
    },
    [isSignUp, loginInput, pwdInput, confirmInput]
  );
  return {
    loginInput,
    pwdInput,
    confirmInput,
    isFormValid,
    inputsRef,
    updateFormState
  };
}
