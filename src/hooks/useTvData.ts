import { Header } from "@model/index";
import { TvItem, ApisData } from "./../model/tv";
import { FormatedListApi, ApiInterface } from "./../model/api";
import { TV_ENDPOINT, TV_SELECTION_ENDPOINT } from "./../shared/const";
import { useState, useEffect } from "react";
import { PROTECTED_API } from "@interceptor/protected";
import { displayData } from "@shared/utils";
import { AxiosResponse } from "axios";

async function getHeaders(): Promise<Array<Header>> {
  return PROTECTED_API.get(`${TV_SELECTION_ENDPOINT}`).then(
    (resp: AxiosResponse<ApiInterface<Array<Header>>>) => resp.data.data
  );
}
async function getData<T>(params: string): Promise<Array<T>> {
  return PROTECTED_API.get(`${TV_ENDPOINT}?${params}`).then(
    (response: AxiosResponse<ApiInterface<Array<T>>>) => response.data.data
  );
}
function setTableHeaders<T>(datum: T, headers: Array<Header>): Array<Header> {
  return headers.filter((header: Header) => header.isSelected);
}
async function* getViewData<T extends Object>(
  params: string
): AsyncIterableIterator<{
  headers: Array<Header>;
  data: Array<T>;
}> {
  const headers = await getHeaders();
  const data = await getData<T>(params);
  yield {
    headers,
    data
  };
}
function makeTableDataPayload<T>(apiData: ApisData<T>): FormatedListApi {
  const { data, headers } = apiData;
  return {
    headers: setTableHeaders(data[0], headers),
    allHeaders: headers,
    data: (<Array<T>>data)
      .map((d: T) => ({
        key: Object.keys(d),
        data: d
      }))
      .map((datum: any) => displayData(datum.key, datum.data))
  };
}
export function useData<T extends unknown>(
  queryParams: string
): [FormatedListApi] {
  const [data, setData] = useState(<any>{ headers: [], data: [] });
  useEffect(() => {
    const apiIterator: any = getViewData(queryParams);
    const apiIteration: Promise<{
      value: ApisData<T>;
      done: boolean;
    }> = apiIterator.next();
    apiIteration.then((resolved: { value: ApisData<T>; done: boolean }) =>
      setData(makeTableDataPayload<T>(resolved.value))
    );
  }, [queryParams]);
  return [data];
}
