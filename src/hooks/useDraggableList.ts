import { useEffect, useState } from "react";
import { UseHooks, UseDispatch } from "@model/index";

type useDraggaList<T> = [T, UseDispatch<T>, T, UseDispatch<T>];
type simpleDraggableList<T> = [Array<T>, Array<T>];
export function useSimpleDraggableList<T>(
  list: simpleDraggableList<T>
): useDraggaList<Array<T>> {
  const [useFirst, setUseFirst] = useState([]);
  const [useSecond, setUseSecond] = useState([]);
  useEffect(() => {
    setUseFirst(list[0]);
    setUseSecond(list[1]);
  }, []);
  return [useFirst, setUseFirst, useSecond, setUseSecond];
}
