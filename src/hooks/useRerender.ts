import { useState, useEffect } from "react";

export function useRerender<T>(newState: T, func: Function): [T] {
  const [updatedState, setUpatedState]: [T, Function] = useState(newState);
  useEffect(() => {
    const storeUpdated = func.call(null, newState);
    setUpatedState(storeUpdated);
    //store are mutated, send back store new state
  }, [newState]);
  return [newState];
}
