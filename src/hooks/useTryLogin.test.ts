import { cleanup } from "react-testing-library";
import { mockPostInvalidLogin, mockPostValidLogin } from "@mock/fetch.mock";

describe("grant access toke ", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);

  test("It should return token from API", async () => {
    try {
      const response = await mockPostValidLogin();
      expect(response.token).toEqual("newtoken");
    } catch (error) {
      throw new Error(`Expected a tokenand  didn't get one!`);
    }
  });

  test("It should not return token from API", async () => {
    try {
      const response = await mockPostInvalidLogin();
      throw new Error(`Expected an error and didn't get one!`);
    } catch (error) {
      expect(error.message).toEqual("Account Not Found");
      expect(() => {
        throw new Error();
      }).toThrow();
    }
  });
});
