import { TvItem } from "./../model/tv";
import { ApiInterface } from "./../model/api";
import { cleanup } from "react-testing-library";
import { mockSuccessTvData } from "@mock/fetch.mock";

describe("Use list hook", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  beforeEach(() => {});

  afterEach(cleanup);
  test("It should succcessfuly get list from api ", async () => {
    const apiRes: ApiInterface<Array<TvItem>> = await mockSuccessTvData();

    expect(apiRes.hasOwnProperty("status")).toBe(true);
    expect(apiRes.hasOwnProperty("data")).toBe(true);

    expect(Array.isArray(apiRes.data)).toBe(true);
  });
});
