import { StoreState, Header } from "@model/index";
export const INITIAL_HEADERS: Array<Header> = [
  {
    value: "id",
    viewValue: "id",
    isSelected: false
  },
  {
    value: "name",
    viewValue: "name",
    isSelected: true
  },
  {
    value: "image",
    viewValue: "image",
    isSelected: true
  },
  {
    value: "season",
    viewValue: "season",
    isSelected: true
  },
  {
    value: "number",
    viewValue: "number",
    isSelected: true
  },
  {
    value: "runtime",
    viewValue: "runtime",
    isSelected: true
  },
  {
    value: "airdate",
    viewValue: "airdate",
    isSelected: true
  },
  {
    value: "airstamp",
    viewValue: "airstamp",
    isSelected: false
  },
  {
    value: "airtime",
    viewValue: "airtime",
    isSelected: false
  },
  {
    value: "summary",
    viewValue: "summary",
    isSelected: true
  },
  {
    value: "url",
    viewValue: "details",
    isSelected: true
  },
  {
    value: "_links",
    viewValue: "api",
    isSelected: true
  }
];

export const INIT_WIDTHS: { [prop: string]: number } = {
  airdate: 1,
  airstamp: 1,
  airtime: 1,
  id: 1,
  image: 1,
  name: 2,
  number: 1,
  runtime: 1,
  season: 1,
  summary: 2.5,
  url: 1,
  _links: 1
};

export const INITIAL_STORE_STATE: StoreState = {
  list: {
    headers: [],
    data: [],
    widths: INIT_WIDTHS
  }
};
