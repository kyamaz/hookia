import { SET_WIDTHS } from "./../actions/index";
import { MOCK_LIST_HEADERS } from "./../../fixtures/list.headers.fixtures";
import { MOCK_LIST_DATA } from "@fixtures/list.data.fixtures";
import { SET_LIST, SET_HEADERS } from "@state/actions";
import { listReducer } from "./list.reducer";
import { INITIAL_STORE_STATE, INIT_WIDTHS } from "@state/const";
import { cleanup } from "react-testing-library";
import { MOCK_WIDTHS_CHANGE } from "@mock/data";

describe("list reducers", () => {
  afterEach(cleanup);
  test("it should set defaukt default widths", () => {
    const initialState = INITIAL_STORE_STATE.list;
    expect(initialState).toEqual({
      headers: [],
      data: [],
      widths: INIT_WIDTHS
    });
  });

  test("it should set list state", () => {
    const initialState = INITIAL_STORE_STATE.list;
    const action = {
      type: SET_LIST,
      payload: MOCK_LIST_DATA
    };
    const newState = listReducer(initialState, action);
    expect(newState).toEqual({
      headers: initialState.headers,
      data: action.payload,
      widths: initialState.widths
    });
  });
  test("it should set headers state", () => {
    const initialState = INITIAL_STORE_STATE.list;
    const action = {
      type: SET_HEADERS,
      payload: MOCK_LIST_HEADERS
    };
    const newState = listReducer(initialState, action);
    expect(newState).toEqual({
      headers: action.payload,
      data: initialState.data,
      widths: initialState.widths
    });
  });

  test("it should set widths state", () => {
    const initialState = INITIAL_STORE_STATE.list;
    const action = {
      type: SET_WIDTHS,
      payload: MOCK_WIDTHS_CHANGE
    };
    const newState = listReducer(initialState, action);
    expect(newState).toEqual({
      headers: initialState.headers,
      data: initialState.data,
      widths: action.payload
    });
  });
});
