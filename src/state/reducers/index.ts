import { listReducer } from "./list.reducer";
import { DispatchAction, ValidPayload, StoreState } from "@model/index";

export function reducer(
  state: StoreState,
  action: DispatchAction<ValidPayload>
): StoreState {
  return {
    list: listReducer(state.list, action)
  };
}
