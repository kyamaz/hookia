import { ListHeaders, ListWidths } from "./../../model/list";
import { SET_WIDTHS, SET_LIST, SET_HEADERS } from "./../actions/index";
import { DispatchAction, ValidPayload, StoreListState } from "@model/index";
import { TvItem } from "@model/tv";
export function listReducer(
  state: StoreListState,
  action: DispatchAction<ValidPayload>
): StoreListState {
  switch (action.type) {
    case SET_LIST:
      return { ...state, data: [...(<Array<TvItem>>action.payload)] };
    case SET_HEADERS:
      return { ...state, headers: [...(<ListHeaders>action.payload)] };
    case SET_WIDTHS:
      return { ...state, widths: { ...(<ListWidths>action.payload) } };
    default:
      return state;
  }
}
