/// <reference types="cypress" />

export const ERROR_MSG = {
  required: "this field is required",
  minLength: "this field is too short",
  isEqual: "Fields are not identicale",
  isUrl: "Not a valid url "
};

describe("invalid log", () => {
  it("should prevent submit and display error message", () => {
    cy.visit("/login");
    cy.get("#signin-btn").click();
    cy.get("#login-input[type='text']").type("re");
    cy.get("#login-input[type='text']").clear();
    cy.get("#login-input[type='text']~p").should($p =>
      expect($p.text()).to.equal(ERROR_MSG.required)
    );
    cy.get("#login-submit").should("have.attr", "disabled");
  });
});
describe("invalid password", () => {
  it("should prevent submit and display error message", () => {
    cy.visit("/login");
    cy.get("#signin-btn").click();
    cy.get("#login-input[type='text']").type("toto");
    cy.get("#pwd-input[type='password']").type("zz");
    // Take  snapshot with different browser widths.
    //  cy.compareSnapshot("password error msg");
    cy.get("#pwd-input[type='password']~p").should($p =>
      expect($p.text()).to.equal(ERROR_MSG.minLength)
    );
    cy.get("#login-submit").should("have.attr", "disabled");
  });
});
