/// <reference types="cypress" />

describe("landing page home", () => {
  before(function() {
    cy.visit("/login");

    //@ts-ignore
    cy.login();
    //
  });
  it("should display title", () => {
    cy.visit("/");

    cy.get("h1[data-testid='app-title']").should($title =>
      expect($title.text()).to.equal("Stateless simple react hook starter")
    );
  });
});
