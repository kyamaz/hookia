/// <reference types="cypress" />

describe("landing page tv", () => {
    before(function() {
      cy.visit("/login");
  
      //@ts-ignore
      cy.login();
      //
    });
    it("should display title", () => {
      cy.visit("/tv");
  
      cy.get("h1[data-testid='app-title']").should($title =>
        expect($title.text()).to.equal("Dynamic list sample")
      );
    });
  });
  